# strsha256

## Depends on

* [Crypto++ Library](https://www.cryptopp.com/)

## Test

```
sudo apt install libcrypto++-dev
cd src
make clean
make
./strsha256test hello-world
```

## Usage

1. `sudo apt install libcrypto++-dev`
1. Copy `strsha256.h` into your project.
1. Add `#include "strsha256.h"` into your `cpp` file.
1. Add `-lcrypto++` option into your `g++` command.

