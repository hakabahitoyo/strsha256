#include <iostream>
#include <cassert>

#include "strsha256.h"


using namespace std;


int main (int argc, char **argv)
{
	auto rnd = strsha256::StrRnd256 {false} ();
	cout << rnd << endl;
	cout << strsha256::StrRnd256 {true} () << endl;
	cout << strsha256::StrSha256 {false} (rnd) << endl;
	cout << strsha256::StrSha256 {true} (rnd) << endl;
	cout << endl;
	string a {"03470e580c2609c1037c08760ad203d5009303d4091605f8028b0e7e0e1d0a54"};
	string b {"01d103d70c8301df068307ec06c003da0b53066d0227037e0a9005b706860a71"};
	cout << a << endl;
	cout << b << endl;
	cout << strsha256::StrSha256 {false} (a) << endl;
	cout << strsha256::StrSha256 {false} (a + b) << endl;
	return 0;
}

